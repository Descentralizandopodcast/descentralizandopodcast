---
title: "Episodio 0: Piloto"
date: 2022-01-24T11:00:00+00:00
# weight: 1
# aliases: ["/first"]
tags: ["Piloto"]
author: "Juan Martín"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
comments: true
description: "Episodio piloto para dar los primeros pasos y definir la linea editorial del podcast"
canonicalURL: "https://canonical.url/to/page"
disableHLJS: false # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: false
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShareButtons: ["twitter", "telegram", "whatsapp", "linkedin"]

cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://gitlab.com/Descentralizandopodcast/descentralizandopodcast.gitlab.io/-/tree/main/content"
    Text: "Sugerir cambios" # edit text
    appendFilePath: false # to append file path to Edit link
---

{{<audio src-ogg="https://archive.org/download/episodio-0-descentralizando-podcast/Episodio_0_descentralizando_podcast.ogg" src-mp3="https://archive.org/download/episodio-0-descentralizando-podcast/Episodio_0_descentralizando_podcast.mp3" >}}

Episodio piloto del podcast, en el que vamos a probar como funciona esto de hacer un podcast, primeros pasos en lo que espero sea un camino de aprender más sobre el mundo de las redes descentralizadas y el mundo de podcasting.

Porque de esto va este podcast de aprender sobre redes descentralizas desde un punto de vista técnico, adentrarnos juntos en este mundo como creadores de aplicaciones. Vamos a explorar las posibilidades que nos brindan las redes descentralizadas para transformar nuestros modelos de negocio o para encontrar soluciones que antes eran imposibles.

Música de [Coma-Media](https://pixabay.com/es/users/coma-media-24399569/?tab=audio&utm_source=link-attribution&utm_medium=referral&utm_campaign=audio&utm_content=11254) de [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=11254)