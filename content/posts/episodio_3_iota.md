---
title: "Episodio 3: Red IOTA"
date: 2022-02-22T11:00:00+00:00
# weight: 1
# aliases: ["/first"]
tags: ["IOTA", "Redes descentralizadas"]
author: "Juan Martín"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
comments: true
description: "Episodio donde hablamos sobre la red IOTA, como funciona la red y como podemos construir redes usando IOTA"
canonicalURL: "https://canonical.url/to/page"
disableHLJS: false # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: false
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShareButtons: ["twitter", "telegram", "whatsapp", "linkedin"]

cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://gitlab.com/Descentralizandopodcast/descentralizandopodcast.gitlab.io/-/tree/main/content"
    Text: "Sugerir cambios" # edit text
    appendFilePath: false # to append file path to Edit link
---

{{<audio src-ogg="https://archive.org/download/e-3-red-iota/E3_red_IOTA.ogg" src-mp3="https://archive.org/download/e-3-red-iota/E3_red_IOTA.mp3" >}}

En este capítulo vamos a dejar un poco las redes blockchain para hablar de una red que usa un sistema diferente para almacenar las transacciones, hablamos de la red IOTA, red que preparada para recibir un gran número de transacciones sin tarifas.

Vemos como funciona esta red, que nodos tenemos disponibles para levantar en la red y como podemos desplegar diferentes redes encima de la propia red de IOTA.

Fuentes: 

[https://www.iota.org/](https://www.iota.org/)

[https://wiki.iota.org/learn/about-iota/an-introduction-to-iota](https://wiki.iota.org/learn/about-iota/an-introduction-to-iota)

[https://wiki.iota.org/build/welcome](https://wiki.iota.org/build/welcome)

[https://wiki.iota.org/build/networks/iota-1.5](https://wiki.iota.org/build/networks/iota-1.5)

[https://wiki.iota.org/smart-contracts/overview](https://wiki.iota.org/smart-contracts/overview)