---
title: "Episodio 4: Identidad digital"
date: 2022-03-08T11:00:00+00:00
# weight: 1
# aliases: ["/first"]
tags: ["Identidad", "SSI", "Identidad digital"]
author: "Juan Martín"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
comments: true
description: "Episodio donde hablamos sobre identidad digital, en que consiste, algunos modelos de identidad y como podemos aplicarlo en redes descentralizadas"
canonicalURL: "https://canonical.url/to/page"
disableHLJS: false # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: false
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShareButtons: ["twitter", "telegram", "whatsapp", "linkedin"]

cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://gitlab.com/Descentralizandopodcast/descentralizandopodcast.gitlab.io/-/tree/main/content"
    Text: "Sugerir cambios" # edit text
    appendFilePath: false # to append file path to Edit link
---

{{<audio src-ogg="https://archive.org/download/e4_SSI/e4_SSI.ogg" src-mp3="https://archive.org/download/e4_SSI/e4_SSI.mp3" >}}

En este capitulo vamos a adentrarnos en el mundo de la identidad digital, veremos que consiste este concepto, que modelos de identidad digital podemos encontrar en los diferentes sistemas de información y para acabar nos centraremos en un modelo de identidad descentralizada, el modelo de identidad autosoverana, o Self Sovering Identity (SSI).

La intención de este capitulo es ofrecer contexto sobre que significa la identidad digital de cara a futuros capítulos donde veremos en más detalles como funciona un modelo de identidad descentralizada o como podemos usar estos modelo para dar más control a los usuarios sobre sus datos.

Fuentes: 

[https://ssimeetup.org/](https://ssimeetup.org/)

[https://ec.europa.eu/digital-building-blocks/wikis/display/CEFDIGITAL/EBSI](https://ec.europa.eu/digital-building-blocks/wikis/display/CEFDIGITAL/EBSI)

[https://www.manning.com/books/self-sovereign-identity](https://www.manning.com/books/self-sovereign-identity)


