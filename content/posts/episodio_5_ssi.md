---
title: "Episodio 5: Auto-Identidad Soberana"
date: 2022-03-24T11:00:00+00:00
# weight: 1
# aliases: ["/first"]
tags: ["Identidad", "SSI", "Identidad digital"]
author: "Juan Martín"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
comments: true
description: "En este capitulo hacemos una introducción al mundo de la auto-identidad soberada"
canonicalURL: "https://canonical.url/to/page"
disableHLJS: false # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: false
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShareButtons: ["twitter", "telegram", "whatsapp", "linkedin"]

cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://gitlab.com/Descentralizandopodcast/descentralizandopodcast.gitlab.io/-/tree/main/content"
    Text: "Sugerir cambios" # edit text
    appendFilePath: false # to append file path to Edit link
---

{{<audio src-ogg="https://archive.org/download/e-5-ssi/E5_SSI.ogg" src-mp3="https://archive.org/download/e-5-ssi/E5_SSI.mp3" >}}

En este capitulo hacemos una introducción al mundo de la auto-identidad soberada, en que consiste este modelo de identidad, cuales son sus roles fundamentales, como funciona y que partes la componente.

También veremos algunos ejemplos de como podemos aplicar estos conceptos a distintos escenarios y de esta manera aclarar mejor los flujos que surgen y la multitud de posibilidades que este modelo nos presenta.

Fuentes: 

[https://ssimeetup.org/](https://ssimeetup.org/)

[https://www.w3.org/TR/did-core/]( https://www.w3.org/TR/did-core/)

[https://www.manning.com/books/self-sovereign-identity](https://www.manning.com/books/self-sovereign-identity)


